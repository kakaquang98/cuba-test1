package com.company.cubatest1.web.screens.session;

import com.haulmont.cuba.gui.screen.*;
import com.company.cubatest1.entity.Session;

@UiController("cubatest1_Session.edit")
@UiDescriptor("session-edit.xml")
@EditedEntityContainer("sessionDc")
@LoadDataBeforeShow
public class SessionEdit extends StandardEditor<Session> {
}
package com.company.cubatest1.web.screens.speaker;

import com.haulmont.cuba.gui.screen.*;
import com.company.cubatest1.entity.Speaker;

@UiController("cubatest1_Speaker.browse")
@UiDescriptor("speaker-browse.xml")
@LookupComponent("speakersTable")
@LoadDataBeforeShow
public class SpeakerBrowse extends StandardLookup<Speaker> {
}
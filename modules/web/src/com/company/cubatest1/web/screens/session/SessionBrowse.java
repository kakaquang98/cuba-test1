package com.company.cubatest1.web.screens.session;

import com.haulmont.cuba.gui.screen.*;
import com.company.cubatest1.entity.Session;

@UiController("cubatest1_Session.browse")
@UiDescriptor("session-browse.xml")
@LookupComponent("sessionsTable")
@LoadDataBeforeShow
public class SessionBrowse extends StandardLookup<Session> {
}
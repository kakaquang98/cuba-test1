package com.company.cubatest1.web.screens.speaker;

import com.haulmont.cuba.gui.screen.*;
import com.company.cubatest1.entity.Speaker;

@UiController("cubatest1_Speaker.edit")
@UiDescriptor("speaker-edit.xml")
@EditedEntityContainer("speakerDc")
@LoadDataBeforeShow
public class SpeakerEdit extends StandardEditor<Speaker> {
}
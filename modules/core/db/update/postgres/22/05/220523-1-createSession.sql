create table CUBATEST1_SESSION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TOPIC varchar(255) not null,
    STARRT_DATE timestamp not null,
    DURATION integer not null,
    SPEAKER_ID uuid,
    DESCRIPTION text,
    --
    primary key (ID)
);
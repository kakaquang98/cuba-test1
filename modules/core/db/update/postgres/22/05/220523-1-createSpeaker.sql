create table CUBATEST1_SPEAKER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    FIST_NAME varchar(255) not null,
    LAST_NAME varchar(255) not null,
    EMAIL varchar(1024) not null,
    --
    primary key (ID)
);
package com.company.cubatest1.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Table(name = "CUBATEST1_SPEAKER", indexes = {
        @Index(name = "IDX_CUBATEST1_SPEAKER_LAST_NAME", columnList = "LAST_NAME")
})
@Entity(name = "cubatest1_Speaker")
@NamePattern("%s|lastName")
public class Speaker extends StandardEntity {
    private static final long serialVersionUID = -2288348761477195753L;

    @NotNull
    @Column(name = "FIST_NAME", nullable = false)
    private String fistName;

    @NotNull
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    @NotNull
    @Column(name = "EMAIL", nullable = false, length = 1024)
    @Email
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }
}